import axios from 'axios';
import Cookies from 'js-cookie'
import { getToken } from './auth'

const service = axios.create({
  baseURL: 'http://localhost:9000/api',
  timeout: 5000
});

service.interceptors.request.use(
  config => {
    const token = getToken()
    
    if (token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers['Authorization'] = 'Bearer ' + token
    }
    
    return config;
  },
  error => {
    // console.log(error);
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  response => {
    const res = response.data;
    
    if (res.success !== true) {
      return Promise.reject(res);
    } else {
      return res;
    }
  },
  error => {
    // console.log(error)
    if (error.response && 401 === error.response.status) {
      Cookies.remove('token');
    }
    return Promise.reject(error);
  }
);

export default service
