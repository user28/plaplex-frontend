import Cookies from 'js-cookie'

const tokenKey = 'token'

export function setToken(token) {
  return Cookies.set(tokenKey, token, { expires: 7 })
}

export function getToken() {
  return Cookies.get(tokenKey)
}

export function removeToken() {
  return Cookies.remove(tokenKey)
}
