import request from '../utils/request'

export function check(data) {
    return request({
        url: '/user-excursions/check',
        method: 'post',
        data
    })
}

export function get(id) {
  return request({
    url: `/user-excursions/${id}`,
    method: 'get'
  })
}

export function update(data) {
  const { id } = data;

  return request({
    url: `/user-excursions/${id}`,
    method: 'put',
    data
  })
}

export function create(data) {
    return request({
        url: '/user-excursions/create',
        method: 'post',
        data
    })
}
