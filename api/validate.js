import request from '../utils/request'

export function validateEmail(email) {
    return request({
        url: '/user/exists?email=' + email,
        method: 'get',
    })
}

