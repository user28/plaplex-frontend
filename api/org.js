import request from '../utils/request'

export function create(data) {
    return request({
        url: '/org/create',
        method: 'post',
        data
    })
}

export function get(id) {
    return request({
        url:    `/org/${id}`,
        method: 'get'
    })
}

export function update(data) {
    return request({
        url: `/org/${data.id}`,
        method: 'put',
        data
    })
}

export function login(data) {
    return request({
        url:    `/org/login`,
        method: 'post',
        data
    })
}

