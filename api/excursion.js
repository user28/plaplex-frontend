import request from '../utils/request'

export function create(data) {
  return request({
    url: '/excursion/create',
    method: 'post',
    data
  })
}

export function uploadBackground(data) {
  return request({
    url: '/attachment/upload/excursion-background',
    method: 'post',
    data
  })
}

export function update(id, data) {
  return request({
    url: '/excursion/' + id,
    method: 'put',
    data
  })
}

export function remove(id) {
  return request({
    url: '/excursion/' + id,
    method: 'delete'
  })
}

export function get(id) {
  return request({
    url: `/excursion/${ id }`,
    method: 'get'
  })
}

export function popular() {
  return request({
    url: '/excursion/popular',
    method: 'get'
  })
}

export function recommended() {
  return request({
    url: '/excursion/recommended',
    method: 'get'
  })
}

export function list() {
  return request({
    url: '/excursion/list',
    method: 'get'
  })
}

export function search(search) {
  return request({
    url: `/excursion/search${ search }`,
    method: 'get'
  })
}

export function dateData(id, dateId) {
  return request({
    url: `/excursion/record-data/${id}?time=${dateId}`,
    method: 'get'
  })
}

