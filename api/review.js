import request from '../utils/request'

export function list(excursionId) {
  return request({
    url: `/review?excursionId=${excursionId}`,
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/review/add',
    method: 'post',
    data
  })
}
