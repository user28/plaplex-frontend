import request from '../utils/request'

export function login(data) {
    return request({
        url: '/user/login',
        method: 'post',
        data
    })
}

export function create(data) {
    return request({
        url: '/user/create',
        method: 'post',
        data
    })
}

export function info() {
    return request({
        url: '/user/info',
        method: 'get'
    })
}

export function update(data) {
  return request({
    url: `/user/${data.id}`,
    method: 'put',
    data
  })
}

