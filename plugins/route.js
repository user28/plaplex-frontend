import { getToken } from "../utils/auth";

export default ({ app }) => {
  app.router.beforeEach(async (to, from, next) => {
    const hasToken = getToken()
    if (hasToken) {
      if (to.path === '/login') {
        next({path: '/'})
      } else {
        const hasRole = app.store.getters.id && app.store.getters.id.length > 0

        if (hasRole) {
          next()
        } else {
          // get user info
          try {
            const { id } = await app.store.dispatch('user/getInfo')
            next()
          } catch(error) {
            console.log(error)

            app.store.dispatch('user/logout')
            next({path: '/login'})
          }
        }
      }
    } else {
      app.store.dispatch('user/logout')
      next()
    }
  })
}
