import Cookies from 'js-cookie'

export const state = () => ({
  dark: Cookies.get('dark_theme') || false
});

export const getters = {
  dark: (state) => {
    return state.dark
  }
}

export const mutations = {
  SET_DARK: (state, dark) => {
    state.dark = dark
  }
}

export const actions = {
  // user login
  setDark({ commit }, dark) {
    Cookies.set('dark_theme', dark)
    commit('SET_DARK', dark)
  }
}
