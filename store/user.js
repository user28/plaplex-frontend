import Cookies from 'js-cookie'
import { info, login } from '../api/user'
import { create as createUser, update as updateUser } from '../api/user'
import { create as createOrg, update as updateOrg } from '../api/org'
import { getToken, removeToken, setToken } from '../utils/auth'

export const state = () => ({
  token: Cookies.get('token'),
  name: '',
  avatar: '',
  email: '',
  phone: '',
  id: '',
  role: '',
});

export const getters = {
  token: (state) => {
    return state.token
  },

  name: (state) => {
    return state.name
  },

  avatar: (state) => {
    return state.avatar
  },

  email: (state) => {
    return state.email
  },

  phone: (state) => {
    return state.phone
  },

  id: (state) => {
    return state.id
  },

  role: (state) => {
    return state.role
  }
}

export const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLE: (state, role) => {
    state.role = role
  },
  SET_EMAIL: (state, email) => {
    state.email = email
  },
  SET_PHONE: (state, phone) => {
    state.phone = phone
  },
  SET_ID: (state, id) => {
    state.id = id
  }
}

export const actions = {
  // user login
  login({ commit }, data) {
    const { email, password } = data
    return new Promise((resolve, reject) => {
      login({
        email: email.trim(),
        password: password,
      }).then(response => {
        const { token } = response
        commit('SET_TOKEN', token)
        setToken(token)
        resolve()
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },

  updateOrg({commit}, data) {
    return new Promise((resolve, reject) => {
      updateOrg(data).then(response => {
        const { user } = response
        const { email, avatar, name } = user

        commit('SET_EMAIL', email)
        commit('SET_NAME', name)
        commit('SET_AVATAR', avatar)

        resolve(user)
      }).catch(error => reject(error))
    })
  },

  updateUser({commit}, data) {
    return new Promise((resolve, reject) => {
      updateUser(data).then(response => {
        const { user } = response
        const { email, avatar, name } = user

        commit('SET_EMAIL', email)
        commit('SET_NAME', name)
        commit('SET_AVATAR', avatar)

        resolve(user)
      }).catch(error => reject(error))
    })
  },

  // register new org
  createOrg({ commit }, data) {
    const { email, name, phone, inn, bill, password, passwordConfirmation } = data
    return new Promise((resolve, reject) => {
      createOrg({
        email: email.trim(),
        name,
        inn,
        bill,
        password,
        passwordConfirmation
      }).then(response => {
        const { token } = response
        commit('SET_TOKEN', token)
        setToken(token)
        resolve()
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },

  // register new org
  createUser({ commit }, data) {
    const { email, name, phone, password, passwordConfirmation } = data
    return new Promise((resolve, reject) => {
      createUser({
        email: email.trim(),
        name,
        password,
        passwordConfirmation
      }).then(response => {
        const { token } = response
        commit('SET_TOKEN', token)
        setToken(token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit }) {
    return new Promise((resolve, reject) => {
      info().then(response => {
        const { user } = response

        if (!user) {
          reject('Verification failed, please Login again.')
        }

        const { role, email, phone, name, avatar, avatar_att, id} = user

        commit('SET_ROLE', role)
        commit('SET_ID', id)
        commit('SET_EMAIL', email)
        commit('SET_NAME', name)
        commit('SET_PHONE', phone)
        commit('SET_AVATAR', avatar_att && avatar_att.link)
        commit('SET_TOKEN', getToken())
        resolve(user)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state, dispatch }) {
    return new Promise((resolve, reject) => {
      commit('SET_TOKEN', '')
      commit('SET_ROLE', '')
      removeToken()
      resolve()
    })
  }
}
